;;; helm-tab-bar.el --- this file is not part of GNU Emacs. -*- lexical-binding: t -*-
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
;;
;;; Commentary:
;;
;; Helm interface for Emacs tab bar
;;
;;; Code:

(require 'helm)

(defvar-keymap helm-tab-map
  :parent helm-map
  "M-D" 'helm-tab-delete-command
  "M-R" 'helm-tab-rename-command
  "M-T" 'helm-tab-detach-command)

(defvar helm-tab-actions
  '(("Switch to tab" . tab-bar-switch-to-tab)
    ("Delete tab" . helm-tab-delete)
    ("Rename tab" . helm-tab-rename)
    ("Detach tab" . helm-tab-detach)))

(defun helm-tab-candidates ()
  (mapcar (lambda (tab-name)
            (alist-get 'name tab-name))
          (tab-bar--tabs-recent)))

(defun helm-tab-delete (_)
  (dolist (cand (helm-marked-candidates))
    (tab-bar-close-tab
     (1+ (tab-bar--tab-index-by-name cand)))))

(defun helm-tab-rename (tab-name)
  (tab-bar-rename-tab
   (read-from-minibuffer
    "New name for tab (leave blank for automatic naming): "
    nil nil nil nil tab-name)))

(defun helm-tab-detach (tab-name)
  (let* ((tabs (funcall tab-bar-tabs-function))
         (tab-index (tab-bar--tab-index-by-name tab-name))
         (from-frame (selected-frame))
         (new-frame (make-frame `((name . ,tab-name)))))
    (tab-bar-move-tab-to-frame
     nil from-frame from-number new-frame nil)
    (with-selected-frame new-frame
      (tab-bar-close-tab))))

(helm-make-command-from-action helm-tab-detach-command
  "Detach tab"
  'helm-tab-detach)

(helm-make-command-from-action helm-tab-rename-command
  "Rename tab"
  'helm-tab-rename)

(helm-make-command-from-action helm-tab-delete-command
  "Delete tab"
  'helm-tab-delete)

(defvar helm-tab-bar-source
  (helm-build-sync-source "Tabs"
    :candidates 'helm-tab-candidates
    :action helm-tab-actions
    :keymap helm-tab-map
    :volatile t))

;;;###autoload
(defun helm-tab-switch ()
  (interactive)
  (helm :sources '(helm-tab-bar-source)
        :buffer "*helm tabs*"
        :prompt "Tab: "))

(provide 'helm-tab-bar)
;;; helm-tab-bar.el ends here
